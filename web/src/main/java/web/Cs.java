package web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/cs")
public class Cs extends HttpServlet {
	
	@Override
	public void init() throws ServletException {
		System.out.println("Servlet Iniciado.");
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		PrintWriter out = resp.getWriter();
		
		String login = req.getParameter("usuario");
		String senha = req.getParameter("senha");
		
		out.println("<html>");
		out.println("<head>");
		out.println("</head>");
		out.println("<body>");
		out.println("<h1>Log in</h1>");
		if(login.equals("admin") && senha.equals("1234")) {
			out.println("<h3>Seja bem vindo, " + login + "</h3>");
			HttpSession session = req.getSession();
			out.println("Id sessao: " + session.getId());
			
		}
		else {
			out.println("usuario ou senha incorreto");
			HttpSession session = req.getSession();
			session.invalidate();
		}
		out.println("</body>");
		out.println("</html>");		
	}
	
	@Override
	public void destroy() {
		System.out.println("Servlet destruido.");
	}

}