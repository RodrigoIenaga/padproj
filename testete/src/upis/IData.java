package upis;

public class IData {

	private int dia;
	
	private boolean ehBissexto(int ano) {
		return (ano % 400 == 0) || ((ano % 4 == 0) && (ano % 100 != 0));
	}
	
	private byte getUltimoDia(int mes, int ano) {
		byte ud [] = {0,31,28,31,30,31,30,31,31,30,31,30,31}; 
		
		if(mes == 2 && ehBissexto(ano)) {
			return 29;
		}
		
		return ud[mes];
	}
	
	//criacao
	
	public IData() {
		setAno((byte)1);
		setMes((byte)1);
		setDia((byte)1);
	}
	
	public IData(byte dia, byte mes, short ano) {
		setAno(ano);
		setMes(mes);
		setDia(dia);
	}
	
	//GET
	
	public int getAno() {	
		int ano = dia / 365;
		return ano;
	}
	
	public int getMes() {
		
		int mais = (getAno() - 1)/4;
		int a = dia - (365*(getAno()) + mais);
		int mes = 1;
		
		int check = getAno();
		int bi = 0;
		
		if (ehBissexto(check)) {
			bi = 1;
		}
		
		if ((a > 0) && (a < 32)){
			mes = 1;
		}
		
		else if ((a > 31 + bi) && (a < 61 + bi)){
			mes = 2;
		}
		
		else if ((a > 60 + bi) && (a < 92 + bi)){
			mes = 3;
		}
		
		else if ((a > 91 + bi) && (a < 122 + bi)){
			mes = 4;
		}
		
		else if ((a > 121 + bi) && (a < 153 + bi)){
			mes = 5;
		}
		
		else if ((a > 152 + bi) && (a < 183 + bi)){
			mes = 6;
		}
		
		else if ((a > 182 + bi) && (a < 214 + bi)){
			mes = 7;
		}
		
		else if ((a > 213 + bi) && (a < 245 + bi)){
			mes = 8;
		}
		
		else if ((a > 244 + bi) && (a < 275 + bi)){
			mes = 9;
		}
		
		else if ((a > 274 + bi) && (a < 306 + bi)){
			mes = 10;
		}
		
		else if ((a > 305 + bi) && (a < 336 + bi)){
			mes = 11;
		}
		
		else if ((a > 335 + bi) && (a < 366 + bi)){
			mes = 12;	
		
		}
		
	return mes;
		
	}

	public int getDia() {
		
		int mais = (getAno() - 1)/4;
		int a = dia - (365*(getAno()) + mais);
		int bi = 0;
		if (ehBissexto(getAno())) {
			bi = 1;
		}	
		
		int days [] = {0, 0, 31, 59+bi, 90+bi, 120+bi, 151+bi, 181+bi, 212+bi, 243+bi, 273+bi, 304+bi, 334+bi};
		int day = a - days[getMes()];

		return day;
	}

	//SET
	
	public void setAno(int ano) {
		if(ano >= 1 && ano <= 9999) {
			int mais = (getAno() -1)/4;
			int year = 365*((getAno() - 1) + mais);
			int var = dia - year;
			int maism = (ano - 1)/4;
			dia = var + 365*ano + maism;
		}
	}	
	
	public void setMes(int mes) {
		if (mes > 0 && mes < 13){
			int bi = 0;
			if (ehBissexto(getAno())) {
				bi = 1;
			}
			
			int days [] = {0,0,31, 59+bi, 90+bi, 120+bi, 151+bi, 181+bi, 212+bi, 243+bi, 273+bi, 304+bi, 334+bi};
			int dif = days[mes] - days[getMes()];			
			dia = dia + dif;
		}
	}
	
	public void setDia(int day) {
		byte ultimoDia = getUltimoDia(getMes(),getAno());
		if (day > 0 && day <= ultimoDia) {
			dia = dia - getDia() + day; 			
		}
	}
	//STRING
	
	public String toString() {
		return getDia() + "/" + getMes() + "/" + getAno();
	}
	
	//INCREMENTA 1
	
	public void incrementaAno() {
		int proxAno = getAno()+1;
		int mes = getMes();
			if ((proxAno -1) > 0 && (proxAno -1) < 9999){
				if ((ehBissexto(proxAno) && mes > 2) || (ehBissexto(proxAno -1) && mes < 3)) {
					dia = dia + 366;
				}
				else {
					dia = dia + 365;
				}
			}
	}
	
	public void incrementaMes() {
		int last = getUltimoDia(getMes(), getAno());
		dia = dia + last;
	}
	
	public void incrementaDia() {
		dia = dia + 1;
	}

	//INCREMENTA N
	
	public void incrementaNAno(byte ano) {		
		byte i = 0;
		while (i < ano) {
			incrementaAno();
			i++;
		}
	}
	
	public void incrementaNMes(byte mes) {
		byte i = 0;
		while (i < mes) {
			incrementaMes();
			i++;
		}
	}

	public void incrementaNDia(byte dia) {
		byte i = 0;
		while (i < dia) {
			incrementaDia();
			i++;
		}
	}
	
	public boolean equals(Object obj) {
		
		if(this == obj)
			return true;
		
		if(obj == null || obj.getClass()!= this.getClass())
			return false;
		
		IData d = (IData) obj;
		
		return this.getAno() == d.getAno() && 
				this.getMes() == d.getMes() && 
				this.getDia() == d.getDia();
	}	
	
	public boolean lt(Object obj) {
		
		IData d = (IData) obj;
		
		if (this.getDia() < d.getDia()){
			return true;
		}
		else
			return false;
	}
	
	public boolean le(Object obj) {
		
		return this.equals(obj) || this.lt(obj);
	}
	
	public boolean ge(Object obj) {
		
		return !(this.lt(obj));
	
	}
	
	public boolean gt(Object obj) {
		
		return !(this.le(obj));
	}

	public int compare(IData d1, IData d2) {
		if(d1.lt(d2)) return -1;
		if(d1.equals(d2)) return 0;
		return 1;
	}
}

