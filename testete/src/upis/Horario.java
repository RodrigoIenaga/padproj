package upis;

public class Horario {

		private byte hora; 		// {0 .. 23}
		private byte minuto; 	// {0 .. 59}
		private byte segundo; 	// {0 .. 59}
		
		public Horario() {
			setHora((byte)0);
			setMinuto((byte)0);
			setSegundo((byte)0);
		}
		
		public Horario(byte hora, byte minuto, byte segundo){
			setHora(hora);
			setMinuto(minuto);
			setSegundo(segundo);
		}
		
		public Horario(int hora, int minuto, int segundo) {
			this((byte)hora, (byte)minuto, (byte)segundo);
		}

		public Horario(Horario horario) {
			this(horario.getHora(), horario.getMinuto(), horario.getSegundo());
		}

		public void setHora(byte hora) {
			
			if(hora >= 0 && hora <= 23) {
				this.hora = hora;
			}
		}
		
		public byte getHora() {
			return this.hora;
		}		
	
		public void setMinuto(byte minuto) {
			if(minuto >= 0 && minuto <= 59) {
				this.minuto = minuto;
			}
		}
		
		public byte getMinuto() {
			return this.minuto;
		}
		
		public void setSegundo(byte segundo) {
			if(segundo >= 0 && segundo <= 59) {
				this.segundo = segundo;
			}
		}
		
		public byte getSegundo() {
			return this.segundo;
		}
		
		public String toString() {
			return getHora() + ":" + getMinuto() + ":" + getSegundo();
		}	

		public void incrementaSegundo() {
			
			byte s = (byte)(segundo + 1);
			
			if(s == 60) {
				segundo = 0;
				incrementaMinuto();
			}else {
				segundo = s;
			}
		}
		
		public void incrementaMinuto() {
			byte m = (byte)(minuto + 1);
			
			if(m == 60) {
				minuto = 0;
				incrementaHora();
			}else {
				minuto = m;
			}
		}

		public void incrementaHora() {
			byte h = (byte)(hora + 1);
			
			if(h == 24) {
				hora = 0;
			}else {
				hora = h;
			}			
		}
		
		public boolean ehUltimoHorario() {
			return hora == 23 && minuto == 59 && segundo == 59;
		}

		public boolean ehPrimeiroHorario() {
			return hora == 0 && minuto == 0 && segundo == 0;
		}
		
		
		public void incrementaNSegundo(byte n) {
			byte contador = 0;
			while (contador < n) {
				incrementaSegundo();
				contador++;
			}
		}
		public void incrementaNMinuto(byte n) {
			byte contador = 0;
			while (contador < n) {
				incrementaMinuto();
				contador++;
			}
		}
		public void incrementaNHora(byte n) {
			byte contador = 0;
			while (contador < n) {
				incrementaHora();
				contador++;
			}
		}
		
		private boolean equalsHora(Horario hr) {
			return this.getHora() == hr.getHora();
		}

		private boolean equalsSegundo(Horario hr) {
			return this.getSegundo() == hr.getSegundo();
		}
		
		private boolean equalsMinuto(Horario hr) {
			return this.getMinuto() == hr.getMinuto();
		}

		public boolean equals(Object obj) {
			
			 Horario hr = (Horario) obj;
			 
			 return equalsHora(hr) && 
					equalsMinuto(hr) &&
					equalsSegundo(hr);
		}
		
		public boolean lt(Object obj) {
			
			Horario hr = (Horario) obj;
			
			if(
				(this.getHora() < hr.getHora()) ||			
				(equalsHora(hr) && this.getMinuto() < hr.getMinuto()) ||
				(equalsHora(hr) && equalsMinuto(hr) && this.getSegundo() < hr.getSegundo())
			)
				return true;
			
			else
				return false;
		}
		
		public boolean le(Object obj) {
			
			return this.equals(obj) || this.lt(obj);
		}
		
		public boolean ge(Object obj) {
			
			return !(this.lt(obj));
		
		}
		
		public boolean gt(Object obj) {
			
			return !(this.le(obj));
		}
		
		public int hash(Horario h) {
			int valor;
			valor = h.segundo + (h.minuto * 60) + (h.hora * 3600);
			return
				valor;
		}
		
		public int compare(Horario o1, Horario o2) {
			if(o1.lt(o2)) return -1;
			if(o1.equals(o2)) return 0;
			return 1;
		}

}

