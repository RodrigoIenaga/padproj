package Padroes;

public interface IFigBiDim {
	
	public double perimetro(); 
	//perimetro trocado de int para double para utilizacao do PI preciso
	public double area();

}
