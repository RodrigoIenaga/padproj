package Padroes;

public class circulo implements IFigBiDim {
	
	private float raio;
	
	private boolean condEx(int c) {
		return c > 0;
	}
	
	public circulo() {
		this.raio = 1;
	}
	
	public circulo(int raio) {
		if(!condEx(raio)) {
			throw new RuntimeException("impossivel construir circulo");
		}
	this.raio = raio;
	}
	
	public float getLado() {
		return raio;
	}
	
	public void setLado(int raio) {
		if(condEx(raio)){
			this.raio = raio;
		}
	}
	
	public double perimetro() {
		double pi = Math.PI;
		return (2*raio*pi);
	}
	
	public double area() {
		double pi = Math.PI;
		return (pi*raio*raio);
	}
	
	public String toString() {
		return("(" + this.raio + ")");
	}
}
