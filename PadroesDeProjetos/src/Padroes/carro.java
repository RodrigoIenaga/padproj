package Padroes;

public class carro implements IVeiculoAutomotor{
	
	private int rodas;
	private String Objetivo;
	
	public carro() {
		rodas = 4;
		Objetivo = "Transporte de passageiro, ate 8 pessoas";
	}
	
	public int getRoda() {
		return rodas;
	}
	
	public String getObj(){
		return Objetivo;
	}
	
	public int qqtrodas() {
		int r = getRoda();
		return r;
	}
	
	public String objetivo() {
		String s = getObj();
		return s;
	}
}
